from django.urls import include,path
from . import views
from django.shortcuts import render
from django.conf.urls import url

urlpatterns = [
    path('',views.main,name='main'),
    path('landing/', views.landing,name='landing'),
]
